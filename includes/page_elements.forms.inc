<?php
/* * ***************************************** FORM HOOKS ***************************************************** */

/*
 * Implements hook_form_alter().
 * - Uncomment in order to use form alter */
//function page_elements_form_alter(&$form, &$form_state, $form_id) {
//}

/**
 * Implements hook_form_FORM_ID_alter.
 * Make adjustments to the page_element node edit form
 */
function page_elements_form_page_element_node_form_alter(&$form, &$form_state) {
  /*
   * Add states to the relevant form elements and hide
   * the fields according to the select list current value
   */
  $form['field_pe_media']['#states'] = array(
    'visible' => array(
      '#edit-field-pe-type-und' => array(
        array('value' => 'media')
      )
    )
  );

  $form['field_pe_view']['#states'] = array(
    'visible' => array(
      '#edit-field-pe-type-und' => array(
        array('value' => 'view')
      )
    )
  );

  $form['field_pe_text']['#states'] = array(
    'visible' => array(
      '#edit-field-pe-type-und' => array(
        array('value' => 'text')
      )
    )
  );

  $form['field_pe_link']['#states'] = array(
    'visible' => array(
      '#edit-field-pe-type-und' => array(
        array('value' => 'media'),
        array('value' => 'title'),
        array('value' => 'text')
      )
    )
  );

  $form['field_pe_parallax']['#states'] = array(
    'visible' => array(
      '#edit-field-pe-type-und' => array(
        array('value' => 'media')
      )
    )
  );

  $form['field_pe_entity_reference']['#states'] = array(
    'visible' => array(
      '#edit-field-pe-type-und' => array(
        array('value' => 'entity_reference')
      )
    )
  );
}