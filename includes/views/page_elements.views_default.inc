<?php
/**
 * @file
 * Views defaults by the customization module.
 */

/**
 * Implements hook_views_default_views().
 */
function page_elements_views_default_views() {
  $views = array();

  // Add view to list of views to provide.
//  $views[$view->name] = $view;

  // At the end, return array of default views.
  return $views;
}