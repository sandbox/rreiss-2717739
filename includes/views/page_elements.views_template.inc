<?php
/**
 * @file
 * Views templates by the customization module.
 */

/**
 * Implements hook_views_template().
 */
function page_elements_views_templates() {
  $views = array();

  // Add view to list of views to provide.
//  $views[$view->name] = $view;
  // At the end, return array of default views.
  return $views;
}