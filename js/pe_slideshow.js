(function ($) {
  /*
   * Custom slideshow functionality
   *
   */
  Drupal.behaviors.prSlideshow = {
    attach: function (context, settings) {
      var slideContainer = $('.pe-slideshow .field--name-field-pe-media .field__items', context);
      var slide = $('.field__item', slideContainer);
      if (slide.length > 1) {
        /**
         * @todo Have a per slideshow settings form
         * @see http://bxslider.com/
         */
        slideContainer.bxSlider({
          mode: 'fade',
          infiniteLoop: true,
          responsive: true,
          speed: 500,
          pause: 5000,
          autoHover: true,
          controls: false,
          auto: true
        });
      }
    }
  };
})
(jQuery);