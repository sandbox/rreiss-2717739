<?php
/**
 * @file
 * pe_feature.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function pe_feature_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-page_element-field_pe_entity_reference'.
  $field_instances['node-page_element-field_pe_entity_reference'] = array(
    'bundle' => 'page_element',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'יישות פנימית אחרת באתר, כמו טופס, עמוד, סוגיה, וכו\'',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'entityreference',
        'settings' => array(
          'links' => 0,
          'view_mode' => 'page_element',
        ),
        'type' => 'entityreference_entity_view',
        'weight' => 2,
      ),
      'page_element' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_pe_entity_reference',
    'label' => 'תצוגת יישות',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'entityreference',
      'settings' => array(
        'match_operator' => 'CONTAINS',
        'path' => '',
        'size' => 60,
      ),
      'type' => 'entityreference_autocomplete',
      'weight' => 10,
    ),
  );

  // Exported field_instance: 'node-page_element-field_pe_link'.
  $field_instances['node-page_element-field_pe_link'] = array(
    'bundle' => 'page_element',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'הקישור "יעטוף" את רכיב העמוד',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'link',
        'settings' => array(),
        'type' => 'link_default',
        'weight' => 4,
      ),
      'page_element' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 5,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 5,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_pe_link',
    'label' => 'קישור',
    'required' => 0,
    'settings' => array(
      'absolute_url' => 0,
      'attributes' => array(
        'class' => '',
        'configurable_class' => 0,
        'configurable_title' => 1,
        'rel' => '',
        'target' => 'user',
        'title' => '',
      ),
      'display' => array(
        'url_cutoff' => 80,
      ),
      'enable_tokens' => 1,
      'rel_remove' => 'default',
      'title' => 'none',
      'title_label_use_field_label' => 0,
      'title_maxlength' => 128,
      'title_value' => '',
      'url' => 0,
      'user_register_form' => FALSE,
      'validate_url' => 1,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'link',
      'settings' => array(),
      'type' => 'link_field',
      'weight' => 9,
    ),
  );

  // Exported field_instance: 'node-page_element-field_pe_media'.
  $field_instances['node-page_element-field_pe_media'] = array(
    'bundle' => 'page_element',
    'deleted' => 0,
    'description' => 'בשדה זה ניתן להשתמש ב:<br>
<ul>
<li>וידאו / תמונה בודד/ת</li>
<li>מספר וידאו / תמונות שיופיעו כמצגת</li>
</ul>',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'file_entity',
        'settings' => array(
          'file_view_mode' => 'default',
        ),
        'type' => 'file_rendered',
        'weight' => 0,
      ),
      'page_element' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 4,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 4,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_pe_media',
    'label' => 'מדיה',
    'required' => 0,
    'settings' => array(
      'description_field' => 0,
      'file_directory' => 'media/images/page_element/field_pe_media',
      'file_extensions' => 'png jpg jpeg gif',
      'max_filesize' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'media',
      'settings' => array(
        'allowed_schemes' => array(
          'public' => 'public',
          'youtube' => 'youtube',
        ),
        'allowed_types' => array(
          'audio' => 0,
          'document' => 0,
          'image' => 'image',
          'video' => 'video',
        ),
        'browser_plugins' => array(
          'media_default--media_browser_1' => 0,
          'media_default--media_browser_my_files' => 0,
          'media_internet' => 0,
          'upload' => 0,
        ),
      ),
      'type' => 'media_generic',
      'weight' => 5,
    ),
  );

  // Exported field_instance: 'node-page_element-field_pe_parallax'.
  $field_instances['node-page_element-field_pe_parallax'] = array(
    'bundle' => 'page_element',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => 'האם להשתמש באפקט Parallax',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 6,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_pe_parallax',
    'label' => 'Parallax',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 1,
      ),
      'type' => 'options_onoff',
      'weight' => 6,
    ),
  );

  // Exported field_instance: 'node-page_element-field_pe_text'.
  $field_instances['node-page_element-field_pe_text'] = array(
    'bundle' => 'page_element',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'שדה טקסט מותאם אישית',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 3,
      ),
      'page_element' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 3,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 3,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_pe_text',
    'label' => 'טקסט',
    'required' => 0,
    'settings' => array(
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 8,
    ),
  );

  // Exported field_instance: 'node-page_element-field_pe_type'.
  $field_instances['node-page_element-field_pe_type'] = array(
    'bundle' => 'page_element',
    'default_value' => array(
      0 => array(
        'value' => 'media',
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 5,
      ),
      'page_element' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_pe_type',
    'label' => 'סוג הרכיב',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'node-page_element-field_pe_view'.
  $field_instances['node-page_element-field_pe_view'] = array(
    'bundle' => 'page_element',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'בחירה בתצוגה ייחודית המתאימה לרכיב הספציפי הזה',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'viewfield',
        'settings' => array(),
        'type' => 'viewfield_default',
        'weight' => 1,
      ),
      'page_element' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_pe_view',
    'label' => 'תצוגה ייחודית',
    'required' => 0,
    'settings' => array(
      'allowed_views' => array(
        'admin_views_file' => 0,
        'admin_views_node' => 0,
        'admin_views_user' => 0,
        'media_default' => 0,
      ),
      'force_default' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'viewfield',
      'settings' => array(),
      'type' => 'viewfield_select',
      'weight' => 7,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Parallax');
  t('בחירה בתצוגה ייחודית המתאימה לרכיב הספציפי הזה');
  t('בשדה זה ניתן להשתמש ב:<br>
<ul>
<li>וידאו / תמונה בודד/ת</li>
<li>מספר וידאו / תמונות שיופיעו כמצגת</li>
</ul>');
  t('האם להשתמש באפקט Parallax');
  t('הקישור "יעטוף" את רכיב העמוד');
  t('טקסט');
  t('יישות פנימית אחרת באתר, כמו טופס, עמוד, סוגיה, וכו\'');
  t('מדיה');
  t('סוג הרכיב');
  t('קישור');
  t('שדה טקסט מותאם אישית');
  t('תצוגה ייחודית');
  t('תצוגת יישות');

  return $field_instances;
}
