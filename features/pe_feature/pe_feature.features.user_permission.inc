<?php
/**
 * @file
 * pe_feature.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function pe_feature_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create page_element content'.
  $permissions['create page_element content'] = array(
    'name' => 'create page_element content',
    'roles' => array(
      'Content Manager' => 'Content Manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any page_element content'.
  $permissions['delete any page_element content'] = array(
    'name' => 'delete any page_element content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own page_element content'.
  $permissions['delete own page_element content'] = array(
    'name' => 'delete own page_element content',
    'roles' => array(
      'Content Manager' => 'Content Manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any page_element content'.
  $permissions['edit any page_element content'] = array(
    'name' => 'edit any page_element content',
    'roles' => array(
      'Content Manager' => 'Content Manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own page_element content'.
  $permissions['edit own page_element content'] = array(
    'name' => 'edit own page_element content',
    'roles' => array(),
    'module' => 'node',
  );

  return $permissions;
}
