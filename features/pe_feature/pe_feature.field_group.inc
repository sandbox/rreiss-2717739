<?php
/**
 * @file
 * pe_feature.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function pe_feature_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_pe_content|node|page_element|form';
  $field_group->group_name = 'group_pe_content';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'page_element';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Content',
    'weight' => '2',
    'children' => array(
      0 => 'field_pe_entity_reference',
      1 => 'field_pe_link',
      2 => 'field_pe_media',
      3 => 'field_pe_text',
      4 => 'field_pe_view',
      5 => 'field_pe_parallax',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-pe-content field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_pe_content|node|page_element|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');

  return $field_groups;
}
