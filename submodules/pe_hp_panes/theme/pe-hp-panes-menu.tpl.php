<?php
/**
 * @file Homepage panes menu
 */
?>
<nav id="homepage-panes-menu">
  <ul class="menu">
    <?php foreach ($items as $nid => $item): ?>
      <li class="menu-item">
        <a href="/#pe-<?php print $nid; ?>" title="<?php print t('Jump to @node-title', array('@node-title' => $item)); ?>" class="menu-link">
          <?php print $item; ?>
        </a>
      </li>
    <?php endforeach; ?>
  </ul>
</nav>

