(function ($) {
  Drupal.behaviors.hpPanesSwiper = {
    attach: function (context, settings) {
      var body = $('body');
      var header = $('.l-header');
      var pageElements = $('.node--page-element--full');

      /**
       * Add the current slide functionality
       */
      $(window).load(function() {
        if (window.location.hash) {
          var currentSlide = pageElements.filter(window.location.hash);
          scrollToSlide(currentSlide);
        }
        else {
          var firstSlide = pageElements.first();
          var firstSlideId = '/#' + firstSlide.attr('id');

          firstSlide.addClass('active-page-element was-active-page-element');
          setActiveMenuItem(firstSlideId);
        }
      });

      var scrollTimer;
      document.addEventListener('scroll', function () {
        clearTimeout(scrollTimer);
        scrollTimer = setTimeout(updateCurrentSlideClass, 50);
      });

      /**
       * Eran
       * @todo Get the excluded nids from the back-end
       * @param slide
       * @returns {boolean}
       */
      var isExcludedSlide = function(slide) {
        var excludedNids = ['3','8', '4','9'];
        var slideId = slide[0]['id'].replace(/\D/g,'');
        return (excludedNids.indexOf(slideId) >= 0);
      };

      /**
       * Function to update the current slide class to active / not active
       */
      function updateCurrentSlideClass() {
        pageElements.each(function() {
          var $that = $(this);
          var tolerance = 0;

          if ($that.is(':in-viewport(' + tolerance + ')')) {
            $that.addClass('active-page-element was-active-page-element');

            var slideId = '/#' + $that.attr('id');
            // Set location fragment if not excluded slide
            if (!isExcludedSlide($that)) {
              setLocationFragment($that.data('hash'));
            }

            // Update the active menu item if there's a slideId
            if (slideId) {
              setActiveMenuItem(slideId);
            }

            $that.trigger('paneScrollChange');
          }
          else {
            $that.removeClass('active-page-element');
          }
        });
      }


      function setActiveMenuItem(slideId) {
        var menuItems = $('#homepage-panes-menu .menu-item');
        menuItems.removeClass('active-nav');

        var navAnchor = $('a[href="' + slideId + '"]');
        var navListItem = navAnchor.closest('li');

        navListItem.addClass('active-nav');
      }

      /**
       * Add the next slide button behavior
       */
      $('.next-slide-btn').on('click', function() {
        var nextSlide = $(this).closest('.views-row').next().find('.node--page-element');
        scrollToSlide(nextSlide);
      });

      /* Add the navigation menu behavior */
      var menuLink = $('#homepage-panes-menu .menu-link', context);
      menuLink.on('click', function (e) {
        e.preventDefault();
        var $that = $(this);
        /* Switch between active and inactive navigation */
        var activeNavClass = 'active-nav';
        var lastActiveNav = $('.' + activeNavClass);
        var newActiveNav = $that.closest('li');
        lastActiveNav.removeClass(activeNavClass);
        newActiveNav.addClass(activeNavClass);

        var href = $that.attr('href');
        var id = href.substr(href.indexOf('#') + 1, 10);
        var newSlide = $('#' + id);

        scrollToSlide(newSlide, true); // Scroll to the slide and force the scroll, even if excluded slide
      });

      var scrollToSlide = function(slideObj, force) {
        if (slideObj.length === 0 || (isExcludedSlide(slideObj) && typeof(force) === 'undefined')) {
          return ;
        }

        var scrollableContainer = $(window);
        var header = $('header');
        var isStickyHeader = header.css('position') === 'fixed' ? true : false;
        var topOffeset = isStickyHeader ? header.outerHeight() : 0;
        var topY = slideObj.offset().top - topOffeset;

        TweenMax.to(scrollableContainer, 1, {
          scrollTo: {
            y: topY,
            autoKill: true
          },
          onComplete: function () {
            /*
             * Recheck the header's height on complete, because maybe it was changed during the scroll
             * @todo Find a better way to do it..
             */
            topOffeset = isStickyHeader ? header.outerHeight() : 0;
            var topY = slideObj.offset().top - topOffeset;
            TweenMax.to(scrollableContainer, 0.3, {
              scrollTo: {y: topY, autoKill: true}
            });
          }
        });
      };

      var setLocationFragment = function (id) {
        if(history.pushState) {
          history.pushState(null, null, '#' + id);
        }
        else {
          location.hash = '#' + id;
        }
      };
    }
  };

})(jQuery);