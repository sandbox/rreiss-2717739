(function ($) {
  Drupal.behaviors.hpPanesSwiper = {
    attach: function (context, settings) {
      var body = $('body');
      var header = $('.l-header');

      hpSwiper = new Swiper('#block-pe-hp-panes-page-elements-hp-panes', {
        slideClass: 'node--page-element',
        wrapperClass: 'block__content',
        direction: 'vertical',
        slidesPerView: 0.75,
        freeMode: true,
        keyboardControl: true,
        mousewheelControl: true,
        speed: 1200,
        mousewheelSensitivity: 2,
        nextButton: '.swiper-next',
        a11y: true,
        prevSlideMessage: Drupal.t('Previous slide'),
        nextSlideMessage: Drupal.t('Next slide'),
        hashnav: true,
        onSlideChangeStart: function (mySwiper) {
          mySwiper.lockSwipes();
          setTimeout(function () {
            mySwiper.unlockSwipes();
          }, 600);
        },
        onProgress: function (mySwiper) {
          if (mySwiper.isEnd) {
            body.addClass('last-slide-reached');
          }
          else {
            body.removeClass('last-slide-reached');
          }

          if (mySwiper.isBeginning) {
            header.removeClass('smaller');
            body.removeClass('smaller-header');
            body.addClass('first-slide');
          }
          else {
            header.addClass('smaller');
            body.addClass('smaller-header');
            body.removeClass('first-slide');
          }
        }
      });

      // Create assoc. array of the slides
      var slides = [];
      $('.block--pe-hp-panes-page-elements-hp-panes .node--page-element').each(function (i) {
        slides[$(this).attr('data-hash')] = i;
      });

      var $menu = $('#block-pe-hp-panes-page-elements-hp-panes-menu', context);
      $('.menu-link', $menu).each(function () {
        $(this).on('click', function (e) {
          var href = $(this).attr('href');
          var id = href.substr(href.indexOf('#') + 1, 10);
          if (slides[id]) {
            e.preventDefault();
            hpSwiper.slideTo(slides[id]);
          }
        });
      });
    }
  };

})(jQuery);

