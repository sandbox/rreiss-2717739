<?php
/**
 * @file
 * pe_homepage_feature.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function pe_homepage_feature_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'update homepage_panes entityqueue'.
  $permissions['update homepage_panes entityqueue'] = array(
    'name' => 'update homepage_panes entityqueue',
    'roles' => array(
      'Content Manager' => 'Content Manager',
    ),
    'module' => 'entityqueue',
  );

  return $permissions;
}
