<?php
/**
 * @file
 * pe_homepage_feature.entityqueue_default.inc
 */

/**
 * Implements hook_entityqueue_default_queues().
 */
function pe_homepage_feature_entityqueue_default_queues() {
  $export = array();

  $queue = new EntityQueue();
  $queue->disabled = FALSE; /* Edit this to true to make a default queue disabled initially */
  $queue->api_version = 1;
  $queue->name = 'homepage_panes';
  $queue->label = 'Homepage Panes';
  $queue->language = 'he';
  $queue->handler = 'simple';
  $queue->target_type = 'node';
  $queue->settings = array(
    'target_bundles' => array(
      'page_element' => 'page_element',
    ),
    'min_size' => '1',
    'max_size' => '0',
    'act_as_queue' => 1,
  );
  $export['homepage_panes'] = $queue;

  return $export;
}
